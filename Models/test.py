import joblib
from sklearn.model_selection import GridSearchCV
from sklearn.pipeline import Pipeline
from sklearn.naive_bayes import MultinomialNB
from sklearn.feature_extraction.text import TfidfVectorizer
import nltk
import re
from nltk.stem import PorterStemmer
from nltk.corpus import stopwords

import re


def preprocessor(text):
    text = re.sub("<[^>]*>", "", text)
    emoticons = re.findall("(?::|;|=)(?:-)?(?:\)|\(|D|P)", text)
    # Remove any non-word character and append the emoticons,
    # removing the nose character for standarization. Convert to lower case
    text = (
        re.sub("[\W]+", " ", text.lower()) + " " + " ".join(emoticons).replace("-", "")
    )

    return text


porter = PorterStemmer()


def tokenizer(text):
    return text.split()


def tokenizer_porter(text):
    return [porter.stem(word) for word in text.split()]


stop = stopwords.words("english")

tfidf = TfidfVectorizer


text = ["I love you"]
model = joblib.load("Models/Finished Models/MultinominalNB/Multinominal.joblib")

print(model.predict(text))
